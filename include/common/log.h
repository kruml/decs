#ifndef LOG_H
#define LOG_H

#include <stdio.h>

#define LOG_DEBUG(...) printf(__VA_ARGS__);

#define LOG_INFO(X) printf("INFO: %s\n", X);
#define LOG_ERROR(X) printf("ERROR: %s\n", X);

#endif // LOG_H
