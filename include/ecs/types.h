#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H
#include <stdint.h>

typedef uint64_t DecsId;
typedef DecsId EntityId;
typedef DecsId SystemId;
typedef DecsId ComponentId;
typedef DecsId Signature;

#endif // COMMON_TYPES_H
