#include "ecs/system.h"
#include "ecs/decs.h"
#include "ecs/entity.h"
#include "ecs/types.h"

#include "data_structures/sparse_set.h"

#include "common/data_structures/sparse_set.h"
#include "common/data_structures/stack.h"
#include "common/defines.h"

#include <assert.h>

/********************************************************************
 *                          Private types                           *
 ********************************************************************/
#define STARTING_SYSTEMS_BUF 100

STACK_DECLARE(system_id_stack, SystemId)
STACK_DEFINE(system_id_stack, SystemId)

struct System {
  SystemUpdateCallback update;
  Signature signature;
  EntityIterator it;
};

static system_id_stack available_ids = {.head = NULL, .nodes = 0};
static SparseSet **system_entity_list = NULL;

/********************************************************************
 *                          Private functions                       *
 ********************************************************************/

DecsStatus system_module_init() {
  SystemId i = STARTING_SYSTEMS_BUF;
  while (1) {
    system_id_stack_push(&available_ids, i);
    if (i == 0) {
      break;
    }
    i--;
  }
}

STATIC_INLINE DecsStatus system_update(System *system, float dt) {
  return system->update(dt);
}

void system_list_scale_internal(SystemList *list) {
  list->capacity *= 2;
  list->systems = realloc(list->systems, list->capacity * sizeof(System *));
}

/********************************************************************
 *                          Public functions                       *
 ********************************************************************/

void system_list_create(SystemList *list) {
  system_module_init();
  system_entity_list = malloc(sizeof(SparseSet *) * STARTING_SYSTEMS_BUF);
  list->capacity = STARTING_SYSTEMS_BUF;
  list->systems = malloc(list->capacity * sizeof(System *));
  list->active_systems = 0;
}

void system_list_destroy(SystemList *list) { free(list->systems); }

// TODO: Dont think this works
SystemId system_add(SystemList *list, System *system) {
  if (list->active_systems >= list->capacity) {
    system_list_scale_internal(list);
  }
  SystemId new_id = system_id_stack_pop(&available_ids);
  list->systems[new_id] = system;
  list->active_systems++;
}

DecsStatus system_list_update(SystemList *list, float dt) {
  DecsStatus status = kDecsSuccess;
  for (int i = 0; i < list->active_systems; i++) {
    // TODO: This might be flaws once I add more status members
    status |= system_update(list->systems[i], dt);
  }
  return status;
}

System *system_create(SystemUpdateCallback cb, Signature signature) {
  System *system = malloc(sizeof(System));
  assert(system != NULL);

  system->update = cb;
  system->signature = signature;
  return system;
}
